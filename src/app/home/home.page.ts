import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { PeopleServiceService } from '../people-service.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  results: Observable<any>;

  constructor(private peopleService: PeopleServiceService) { }

  ngOnInit() {
    this.peopleService.searchData().subscribe(res => {
      this.results = res;
    });
  }
}
