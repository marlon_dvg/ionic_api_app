import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PeopleServiceService {
  url = 'http://dinformsuper-pc:8080/api_rest/helloworld';

  constructor(private http: HttpClient) { }

  searchData(): Observable<any> {
    return this.http.get(`${this.url}`, { responseType: 'text' });
  }
}
